
import WSServer from "./WSServer";
import type { IWebSocket } from "../types/types";

export default class WSServerNoAuth extends WSServer {

    readonly isUseAuth = false;

    protected async _initUserModel() {}

    protected async _checkAuth(loginData: IWebSocket.UserLoginData): Promise<IWebSocket.UserData | void> {}
}