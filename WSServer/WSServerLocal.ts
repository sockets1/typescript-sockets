
import WSServer from "./WSServer";
import type { IWebSocket } from "../types/types";

export default class WSServerLocal extends WSServer {

    readonly isUseAuth = true;

    private _usersData: IWebSocket.UserData[] = [];

    initUserData(data: IWebSocket.UserData[]) {
        this._usersData = data;
    }

    protected async _initUserModel() {}

    protected async _checkAuth(loginData: IWebSocket.UserLoginData): Promise<IWebSocket.UserData | void> {
        return this._usersData.find(userData => {
            return userData.username === loginData.username.toLowerCase() && userData.password === loginData.password;
        });
    }
}