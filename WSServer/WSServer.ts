import http from 'node:http';
import {WebSocketServer} from 'ws';
import EventEmitter from 'events';
import {
    Message_AUTH,
    Message_COMMON_MESSAGE,
    Message_EVENT,
    Message_RPC_CALL,
    Message_RPC_RESPONSE,
    MSG_TYPES,
} from "../types/messages";
import type {IWebSocket} from "../types/types";

import {_assert_isWS, _isAuthRequestMessage, _isEventMessage, _isRPCCallMessage,} from "../utils/guards";
import {formMsg, getMsgInfo} from "../utils/common";
import {MSG_FLAGS} from "../utils/flags";
import {
    AUTH_MESSAGE_TEMPLATE,
    MAIN_BUS_ID,
    MAIN_BUS_NAME,
    RPC_NAME__getLoggedUsers,
    RPC_NAME__registerRPC,
    RPC_NAME__unregisterRPC,
    UNKNOWN_ID,
    UNKNOWN_NAME,
} from "../utils/consts";
import {handleEvent} from "../utils/handleEvent";
import {awaitRPCResponse} from "../utils/awaitRPCResponse";

let noAuthIdCounter = 1;

export default abstract class WSServer extends EventEmitter {

    readonly port: number;
    readonly isUseAuth: boolean = false;

    private _httpServer: http.Server;
    private _wsServer: WebSocketServer;

    private _connections: IWebSocket.Connection[] = [];
    private _connectionsMap: Record<number, IWebSocket.Connection> = {};
    private _unregisteredConnections: IWebSocket.UnknownConnection[] = [];
    private _logger: IWebSocket.Logger = console;

    private _rpcsMap: Record<string, IWebSocket.RPCInfo> = {};
    private _rpcsPool: Record<string | number, Promise<void>> = {};

    get authConnectionsAmount() {
        return this._connections.length;
    }

    get nonAuthConnectionsAmount() {
        return this._unregisteredConnections.length;
    }

    constructor(options: IWebSocket.ServerOptions) {
        super();

        const {
            port,
        } = options;

        this.port = port;

        this._httpServer = http.createServer((request) => {
            console.log((new Date()) + ' Received request for ' + request.url);
        });

        this._wsServer = new WebSocketServer({
            server: this._httpServer,
        });
    }

    setLogger(newLogger: IWebSocket.Logger) {
        if (!newLogger) {
            return;
        }

        this._logger = newLogger;
    }

    protected async _onStart() {
        return;
    };

    protected abstract _checkAuth(loginData: IWebSocket.UserLoginData): Promise<IWebSocket.UserData | void>;

    async start() {
        const {
            _httpServer,
            _wsServer,
            port,

            _rpcsMap,
        } = this;

        _httpServer.listen(port, () => {
            this._logger.log(`Started. Listening port ${port}`);
        });
        _wsServer.on('connection', this._onConnectionRequest);

        this.registerRPC(RPC_NAME__registerRPC, (params: { methodName: string, authorID: number }) => {
            const {
                authorID,
                methodName,
            } = params;

            if (_rpcsMap[methodName]) {
                throw new Error(`Trying to register already registered RPC ${methodName}`);
            }

            _rpcsMap[methodName] = {
                id: authorID,
            };
        });
        this.registerRPC(RPC_NAME__unregisterRPC, (params: { methodName: string }) => {
            const {
                methodName,
            } = params;

            if (!_rpcsMap[methodName]) {
                return;
            }

            delete _rpcsMap[methodName];
        });
        this.registerRPC(RPC_NAME__getLoggedUsers, () => {
            return this.getLoggedUsers();
        });

        return this._onStart();
    }

    async stop() {
        const {
            _httpServer,
            _wsServer,

            _rpcsPool,

            _connections,
            _unregisteredConnections,
        } = this;

        _wsServer.removeListener('request', this._onConnectionRequest);

        for (const rpcPromise of Object.values(_rpcsPool)) {
            await rpcPromise;
        }

        for (const connection of _connections) {
            connection.removeAllListeners();
            connection.close();
        }

        for (const connection of _unregisteredConnections) {
            connection.removeAllListeners();
            connection.close();
        }

        if (typeof _httpServer.closeAllConnections === 'function') {
            _httpServer.closeAllConnections();
        }

        await _httpServer.close();

        this._rpcsMap = {};

        this._logger.log(`Stopped`);
    }

    destructor() {
        this.stop().then(() => {
            this._wsServer.close();

            this._wsServer = void 0;
            this._httpServer = void 0;
        });
    }

    getLoggedUsers() {
        const {
            _connections,
        } = this;

        const users: IWebSocket.UsersMap = {};

        for (const connection of _connections) {
            const {
                id,
                username,
            } = connection.user;

            users[id] = username;
        }

        return users;
    }

    getRPCsList(): string[] {
        return Object.keys(this._rpcsMap);
    }

    sendEvent(targetID: number, eventName: string | number, eventData: IWebSocket.ValidMessageData, authorID?: number, tag?: string) {
        const {
            _connectionsMap,
        } = this;
        const connection = _connectionsMap[targetID];

        if (!connection) {
            this._logger.error(`Not found connection with ID ${targetID} to send message`);

            return;
        }

        const msg: Message_EVENT = {
            event: eventName,
            tag,
            data: eventData,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.EVENT,
            targetID,
            flags: authorID === void 0 ? MSG_FLAGS.FROM_SERVER : 0,
        };

        this._send(connection, msg, msgOptions);
    }

    send(id: number, messageData: IWebSocket.ValidMessageData, authorId?: number) {
        const {
            _connectionsMap,
        } = this;
        const connection = _connectionsMap[id];
        const authorName = authorId ? this._getAuthorName(authorId) : MAIN_BUS_NAME;

        if (!connection) {
            this._logger.error(`Not found connection with ID ${id} to send message`);

            return;
        }

        const msg: Message_COMMON_MESSAGE = {
            message: messageData,
            authorName,
            authorId,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.COMMON_MESSAGE,
            targetID: id,
            flags: authorId === void 0 ? MSG_FLAGS.FROM_SERVER : 0,
        };

        this._send(connection, msg, msgOptions);
    }

    broadcastEvent(eventName: string | number, eventData: IWebSocket.ValidMessageData, authorID?: number, tag?: string) {
        const {
            _connections,
        } = this;

        const msg: Message_EVENT = {
            event: eventName,
            tag,
            data: eventData,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.EVENT,
            targetID: UNKNOWN_ID,
            flags: authorID === void 0 ? MSG_FLAGS.FROM_SERVER : 0,
        };

        for (const connection of _connections) {
            if (authorID && connection.id === authorID) {
                continue;
            }

            this._send(connection, msg, { ...msgOptions, targetID: connection.id })
        }
    }

    broadcast(messageData: IWebSocket.ValidMessageData, authorId?: number) {
        const {
            _connections,
            _unregisteredConnections,
        } = this;
        const authorName = authorId ? this._getAuthorName(authorId) : MAIN_BUS_NAME;

        const msg: Message_COMMON_MESSAGE = {
            message: messageData,
            authorName,
            authorId,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.BROADCAST,
            targetID: UNKNOWN_ID,
            flags: authorId === void 0 ? MSG_FLAGS.FROM_SERVER : 0,
        };

        for (const connection of _connections) {
            if (authorId && connection.id === authorId) {
                continue;
            }

            this._send(connection, msg, { ...msgOptions, targetID: connection.id })
        }
        for (const connection of _unregisteredConnections) {
            this._send(connection, msg, msgOptions);
        }
    }

    broadcastAuth(messageData: IWebSocket.ValidMessageData, authorId?: number) {
        const {
            _connections,
        } = this;
        const authorName = authorId ? this._getAuthorName(authorId) : MAIN_BUS_NAME;

        const msg: Message_COMMON_MESSAGE = {
            message: messageData,
            authorName,
            authorId,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.BROADCAST,
            targetID: UNKNOWN_ID,
            flags: authorId === void 0 ? MSG_FLAGS.FROM_SERVER : 0,
        };

        for (const connection of _connections) {
            this._send(connection, msg, { ...msgOptions, targetID: connection.id })
        }
    }

    private _getAuthorName(authorId: number) {
        const {
            _connectionsMap,
        } = this;
        const connectionSource = _connectionsMap[authorId];

        if (connectionSource) {
            return connectionSource.user.username;
        }
        else {
            return UNKNOWN_NAME;
        }
    }

    registerRPC(methodName: string, method: IWebSocket.RPC, authorID = MAIN_BUS_ID): boolean {
        const {
            _rpcsMap,
        } = this;
        const rpcMethodInfo = _rpcsMap[methodName];

        if (rpcMethodInfo) {
            if (rpcMethodInfo.localMethod !== method) {
                this._logger.error(`Method is already exists: ${methodName}`);

                return false;
            }

            // same method was already added
            return true;
        }

        _rpcsMap[methodName] = {
            id: authorID,
            localMethod: method,
        };

        return true;
    }

    unregisterRPC(methodName: string) {
        const {
            _rpcsMap,
        } = this;

        if (!_rpcsMap[methodName]) {
            return;
        }

        delete _rpcsMap[methodName];
    }

    private async _callRPC(uniqueID: string, methodName: string, params?: Record<string | number, unknown>): Promise<Message_RPC_RESPONSE> {
        const {
            _connectionsMap,
            _rpcsMap,
        } = this;
        const rpcMethodInfo = _rpcsMap[methodName];

        if (!rpcMethodInfo) {
            this._logger.error(`Trying to call non-existing RPC method: ${methodName}`);

            return {
                uniqueID,
                methodName,
                isSuccess: false,
                errorText: "Method doesn't exist",
            };
        }

        const isLocal = rpcMethodInfo.id === MAIN_BUS_ID;

        if (isLocal) {
            let result: unknown | void = void 0;

            try {
                result = await rpcMethodInfo.localMethod(params);
            }
            catch (err) {
                this._logger.error(`RPC "${methodName}" error:`, err);

                return {
                    uniqueID,
                    methodName,
                    isSuccess: false,
                    errorText: err instanceof Error ? err.message : err,
                };
            }

            return {
                uniqueID,
                methodName,
                isSuccess: true,
                result,
            };
        }
        else {
            const rpcAuthorConnection = _connectionsMap[rpcMethodInfo.id];

            if (!rpcAuthorConnection) {
                return {
                    uniqueID,
                    methodName,
                    isSuccess: false,
                    errorText: 'RPC handler is disconnected',
                };
            }

            const msg: Message_RPC_CALL = {
                uniqueID,
                methodName,
                params,
            };
            const msgOptions: IWebSocket.FormMessageOptions = {
                type: MSG_TYPES.RPC_CALL,
                targetID: rpcMethodInfo.id,
            };
            const rpcResponseMessage = awaitRPCResponse(rpcAuthorConnection, methodName, uniqueID, this._logger);

            this._send(rpcAuthorConnection, msg, msgOptions);

            return rpcResponseMessage;
        }
    }

    protected async _checkIsAllowedOrigin(origin: string) {
        return origin;
    }

    private _onConnectionRequest = (connection: IWebSocket.UnknownConnection, request: IWebSocket.IncomingRequest) => {
        /*
        if (!this._checkIsAllowedOrigin(request.origin)) {
            // Make sure we only accept requests from an allowed origin
            request.reject();

            this._logger.log(`Connection rejected from origin ${ request.origin }`);
            return;
        }
        */
        // const connection = request.accept(null, request.origin);

        if (this.isUseAuth) {
            this._waitAuth(connection);
        }
        else {
            this._authorizeConnection(connection, this._createUserDataIfNoAuth());
        }

        this._logger.log(`Connection accepted from origin ${ request.url }`);

        this._logConnectionsAmount();
    }

    protected _createUserDataIfNoAuth(): IWebSocket.UserData {
        return {
            id: ++noAuthIdCounter,
            username: 'unknown',
            password: 'noAuthEnabled'
        };
    }

    private _waitAuth(connection: IWebSocket.UnknownConnection) {
        const {
            _unregisteredConnections,
        } = this;

        const listener = async (eventBuffer: IWebSocket.RawData) => {
            const [ msgTechInfo, /* targetId */, authData ] = this._handleEvent(eventBuffer);
            const {
                type: msgType,
            } = getMsgInfo(msgTechInfo);

            if (msgType !== MSG_TYPES.AUTH) {
                this._logger.error(`Received auth message without type AUTH (type: ${msgType}) : ${JSON.stringify(authData)}\n`);

                return;
            }

            if (!_isAuthRequestMessage(authData)) {
                this._logger.error('Received wrong auth message data', JSON.stringify(authData));

                return;
            }
            else {
                const [ username, password ] = authData;
                const userData = await this._checkAuth({
                    username,
                    password,
                });

                if (userData) {
                    this._authorizeConnection(connection, userData);
                }
                else {
                    const authMsg: Message_AUTH = {
                        message: 'Incorrect username or password',
                        isSuccess: false,
                    };
                    const msgOptions: IWebSocket.FormMessageOptions = {
                        targetID: UNKNOWN_ID,
                        type: MSG_TYPES.AUTH,
                        flags: MSG_FLAGS.FROM_SERVER,
                    };

                    this._send(connection, authMsg, msgOptions);

                    this._logger.log(`Failed log attempt with message: ${JSON.stringify(authData)}`);
                }
            }
        }

        _unregisteredConnections.push(connection);

        connection.on('message', listener);
        connection.once('close', () => {
            connection.removeListener('message', listener);

            const iCon = _unregisteredConnections.indexOf(connection);

            _unregisteredConnections.splice(iCon, 1);

            this._logger.log(`Non-Authorized connection closed.`);

            this._logConnectionsAmount();
        });
    }

    private _authorizeConnection(connection: IWebSocket.UnknownConnection, userData: IWebSocket.UserData) {
        const {
            isUseAuth,

            _connections,
            _connectionsMap,
            _unregisteredConnections,
        } = this;
        const {
            id: userId,
            username,
        } = userData;
        const iCon = _unregisteredConnections.indexOf(connection);

        _unregisteredConnections.splice(iCon, 1);

        connection.removeAllListeners();

        if (isUseAuth) {
            this._logger.log(`Authorized user ${username}`);
        }
        else {
            this._logger.log(`Connected user with temp ID ${userId}. (No auth system enabled)`);
        }

        const authMsg: Message_AUTH = {
            message: AUTH_MESSAGE_TEMPLATE,
            isSuccess: true,
            userData: {
                id: userId,
                username,
            },
        };
        const msgOptions: IWebSocket.FormMessageOptions = {
            targetID: userId,
            type: MSG_TYPES.AUTH,
            flags: MSG_FLAGS.FROM_SERVER,
        };

        this._send(connection, authMsg, msgOptions);

        (connection as IWebSocket.Connection).id = userId;
        (connection as IWebSocket.Connection).user = userData;

        connection.on('message', (message) => this._onMessage(connection as IWebSocket.Connection, message));
        connection.once('close', (/*reasonCode, description*/) => {
            connection.removeAllListeners();

            const {
                id: connectionId,
                user: {
                    username,
                },
            } = (connection as IWebSocket.Connection);
            const iCon = _connections.indexOf(connection as IWebSocket.Connection);

            _connections.splice(iCon, 1);

            delete _connectionsMap[connectionId];

            this._logger.log(`Authorized connection closed (ID: ${connectionId}, username: ${username}).`);

            this._logConnectionsAmount();
        });

        _connections.push(connection as IWebSocket.Connection);
        _connectionsMap[userId] = connection as IWebSocket.Connection;

        this._logConnectionsAmount();
    }

    private _onMessage = async (connection: IWebSocket.Connection, eventBuffer: IWebSocket.RawData) => {
        const [ msgTechInfo, targetId, data ] = this._handleEvent(eventBuffer);
        const {
            type,
        } = getMsgInfo(msgTechInfo);

        switch (type) {
            case MSG_TYPES.BROADCAST: {
                return this.broadcast(data, connection.id);
            }
            case MSG_TYPES.COMMON_MESSAGE: {
                if (targetId === MAIN_BUS_ID || targetId === UNKNOWN_ID) {
                    return;
                }

                return this.send(targetId, data, connection.id);
            }
            case MSG_TYPES.RPC_CALL: {
                if (!_isRPCCallMessage(data)) {
                    this._logger.error(`[_onMessage] Received incorrect message to call RPC: ${JSON.stringify(data)}`);

                    return;
                }

                await this._handleRPCCallMessage(connection, data);

                return;
            }
            case MSG_TYPES.EVENT: {
                if (!_isEventMessage(data)) {
                    this._logger.error(`[_onMessage] Received incorrect event message: ${JSON.stringify(data)}`);

                    return;
                }
                if (targetId === MAIN_BUS_ID || targetId === UNKNOWN_ID) {
                    return;
                }

                return this.sendEvent(targetId, data.event, data.data, connection.id, data.tag);
            }
            case MSG_TYPES.SUBSCRIBE: {
                this._logger.error(`[_onMessage] Unsubscribe is not implemented`);

                return;

            }
            case MSG_TYPES.UNSUBSCRIBE: {
                this._logger.error(`[_onMessage] Subscribe is not implemented`);

                return;
            }
            default: {
                return;
            }
        }
    }

    private async _handleRPCCallMessage(connection: IWebSocket.Connection, callMessage: Message_RPC_CALL) {
        const {
            uniqueID,
            methodName,
            params,
        } = callMessage;
        const {
            _rpcsMap,
            _rpcsPool,
        } = this;

        if (methodName === RPC_NAME__registerRPC || methodName === RPC_NAME__unregisterRPC) {
            const result: Message_RPC_RESPONSE | void = {
                uniqueID,
                methodName,
                isSuccess: false,
                errorText: ``,
            };

            if (methodName === RPC_NAME__registerRPC && params?.authorID !== connection.id) {
                result.errorText = `Incorrect authorID`;
            }

            if (methodName === RPC_NAME__unregisterRPC) {
                const methodInfo = _rpcsMap[methodName];

                if (methodInfo && methodInfo.id !== connection.id) {
                    result.errorText = `You're not owner of RPC ${methodName}`;
                }
                else if (!methodInfo) {
                    result.errorText = `RPC method is not found`;
                }
            }

            if (result.errorText.length > 0) {
                this._send(connection, result, {
                    type: MSG_TYPES.RPC_RESPONSE,
                    targetID: connection.id,
                    flags: MSG_FLAGS.FROM_SERVER,
                });

                return;
            }
        }

        const ongoingTask = _rpcsPool[uniqueID];

        if (ongoingTask) {
            await ongoingTask;

            // Must be unreachable due to auto-removing task after completing
            if (_rpcsPool[uniqueID]) {
                this._logger.error(`[_onMessage] Received duplicate uniqueID and can't complete it.`);

                const result: Message_RPC_RESPONSE = {
                    uniqueID,
                    methodName,
                    isSuccess: false,
                    errorText: `Received uniqueID is already used`,
                };

                this._send(connection, result, {
                    type: MSG_TYPES.RPC_RESPONSE,
                    targetID: connection.id,
                    flags: MSG_FLAGS.FROM_SERVER,
                });

                return;
            }
        }

        let rpcCallPromise = this._callRPC(uniqueID, methodName, params).then((rpcResponse) => {
            this._send(connection, rpcResponse, {
                type: MSG_TYPES.RPC_RESPONSE,
                targetID: connection.id,
                flags: MSG_FLAGS.FROM_SERVER,
            });
        });

        _rpcsPool[uniqueID] = rpcCallPromise;

        rpcCallPromise = rpcCallPromise.then(() => {
            delete _rpcsPool[uniqueID];
        });

        return rpcCallPromise;
    }

    private _handleEvent(eventBuffer: IWebSocket.RawData) {
        return handleEvent(eventBuffer, this._logger);
    }

    protected _logConnectionsAmount() {
        const {
            authConnectionsAmount,
            nonAuthConnectionsAmount,
        } = this;

        this._logger.log(`Connections amount: Auth ${ authConnectionsAmount } | ${ nonAuthConnectionsAmount } Non-Auth`);
    }

    private _send(connection: IWebSocket.UnknownConnection, data: IWebSocket.ValidMessageData, options: IWebSocket.FormMessageOptions) {
        _assert_isWS(connection);

        let msg = "";

        try {
            msg = formMsg(data, options);
        }
        catch(err) {
            this._logger.error(`Can't form message`, err);

            return;
        }

        connection.send(msg);
    }
}