// noinspection DuplicatedCode

import WSServerLocal from "../../WSServer/WSServerLocal";
// @ts-ignore
import WebSocket from "ws";
import {formMsg, getMsgInfo} from "../../utils/common";
import {MAIN_BUS_ID, MAIN_BUS_NAME, RPC_NAME__registerRPC, RPC_NAME__unregisterRPC} from "../../utils/consts";
import {DATA_TYPES, Message_RPC_CALL, MSG_TYPES} from "../../types/messages";
import {MSG_FLAGS} from "../../utils/flags";
import {_isRPCResponseMessage} from "../../utils/guards";

const PORT = 8080;
let wsServer: WSServerLocal;

describe('WSServer', () => {
    beforeAll(async () => {
        wsServer = new WSServerLocal({ port: PORT });

        wsServer.initUserData([
            {
                id: 1,
                username: 'user1',
                password: 'pass1',
            },
            {
                id: 2,
                username: 'user2',
                password: 'pass2',
            },
            {
                id: 3,
                username: 'user3',
                password: 'pass3',
            },
        ]);
        await wsServer.start();
    });
    
    afterAll(async () => {
        wsServer.destructor();
    });

    afterEach(async () => {
        await sleep();

        const rpcsList = wsServer.getRPCsList();

        for (const rpcName of rpcsList) {
            if (rpcName !== RPC_NAME__registerRPC && rpcName !== RPC_NAME__unregisterRPC) {
                wsServer.unregisterRPC(rpcName);
            }
        }
    });

    it('Instance of', async () => {
        expect(wsServer).toBeInstanceOf(WSServerLocal);
    });

    describe('Auth', () => {
        it('isUseAuth set to true', async () => {
            expect(wsServer.isUseAuth).toBeTruthy();
        });

        it('Valid auth message', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);

            const serverAnswerPromise = awaitWsMessage(ws);

            // Need to wait for connection to save in local storage
            await sleep();

            const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });
            ws.send(msg);

            const [ msgTechInfo, , serverAnswer ] = JSON.parse(await serverAnswerPromise);

            const msgInfo = getMsgInfo(msgTechInfo);

            expect(serverAnswer.message).toBe('Successfully authorized');
            expect(serverAnswer.isSuccess).toBeTruthy();
            expect(serverAnswer.userData.id).toBe(1);
            expect(serverAnswer.userData.username).toBe('user1');
            expect(msgInfo.type).toBe(MSG_TYPES.AUTH);
            expect(msgInfo.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfo.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);

            ws.close();
        });

        it('Detects auth connections', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);

            const serverAnswerPromise = awaitWsMessage(ws);

            // Need to wait for connection to save in local storage
            await sleep();

            expect(wsServer.nonAuthConnectionsAmount).toBe(1);
            expect(wsServer.authConnectionsAmount).toBe(0);

            const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

            ws.send(msg);

            await serverAnswerPromise

            expect(wsServer.nonAuthConnectionsAmount).toBe(0);
            expect(wsServer.authConnectionsAmount).toBe(1);

            ws.close();
        });

        it('Detects auth disconnections', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);
            const serverAnswerPromise = awaitWsMessage(ws);

            // Need to wait for connection to save in local storage
            await sleep();

            expect(wsServer.nonAuthConnectionsAmount).toBe(1);
            expect(wsServer.authConnectionsAmount).toBe(0);

            const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

            ws.send(msg);

            await serverAnswerPromise;

            expect(wsServer.nonAuthConnectionsAmount).toBe(0);
            expect(wsServer.authConnectionsAmount).toBe(1);

            ws.close();

            // Need to wait for connection removing from local storage
            await sleep();

            expect(wsServer.nonAuthConnectionsAmount).toBe(0);
            expect(wsServer.authConnectionsAmount).toBe(0);
        });

        it('Detects non-auth disconnections', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);

            // Need to wait for connection to save in local storage
            await sleep();

            expect(wsServer.nonAuthConnectionsAmount).toBe(1);
            expect(wsServer.authConnectionsAmount).toBe(0);

            ws.close();

            // Need to wait for connection removing from local storage
            await sleep();

            expect(wsServer.nonAuthConnectionsAmount).toBe(0);
            expect(wsServer.authConnectionsAmount).toBe(0);
        });
    });

    describe('Serverside method calls', () => {
        it('broadcast()', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);
            const ws2 = new WebSocket(`ws://localhost:${PORT}`);
            const serverMessagePromise1 = awaitWsMessage(ws);
            const serverMessagePromise2 = awaitWsMessage(ws2);

            // Need to wait for connection to save in local storage
            await sleep();

            wsServer.broadcast('broadcast message');

            const [ msgTechInfo1, , serverMessage1 ] = JSON.parse(await serverMessagePromise1);
            const [ msgTechInfo2, , serverMessage2 ] = JSON.parse(await serverMessagePromise2);

            const msgInfo1 = getMsgInfo(msgTechInfo1);
            const msgInfo2 = getMsgInfo(msgTechInfo2);

            expect(msgInfo1.type).toBe(MSG_TYPES.BROADCAST);
            expect(msgInfo1.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfo1.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
            expect(serverMessage1.message).toBe('broadcast message');
            expect(serverMessage1.authorName).toBe(MAIN_BUS_NAME);

            expect(msgInfo2.type).toBe(MSG_TYPES.BROADCAST);
            expect(msgInfo2.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfo2.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
            expect(serverMessage2.message).toBe('broadcast message');
            expect(serverMessage2.authorName).toBe(MAIN_BUS_NAME);

            ws.close();
            ws2.close();
        });

        it('broadcastAuth()', async () => {
            const ws = new WebSocket(`ws://localhost:${PORT}`);
            const wsAuth = new WebSocket(`ws://localhost:${PORT}`);

            // Need to wait for connection to save in local storage
            await sleep();

            { // Auth
                const serverAnswerPromise = awaitWsMessage(wsAuth);
                const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                wsAuth.send(msg);

                await serverAnswerPromise;
            }

            let timeoutErrorsCounter = 0;
            const serverMessagePromise = awaitWsMessage(ws)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });
            const serverMessageAuthPromise = awaitWsMessage(wsAuth)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });

            wsServer.broadcastAuth('broadcast message');

            const event = await serverMessagePromise;
            const eventAuth = await serverMessageAuthPromise;

            if (!eventAuth) {
                throw new Error(`Not received event for auth user`);
            }

            const [ msgTechInfoAuth, , serverMessageAuth ] = JSON.parse(eventAuth);

            const msgInfoAuth = getMsgInfo(msgTechInfoAuth);

            expect(event).toBeUndefined();
            expect(timeoutErrorsCounter).toBe(1);

            expect(msgInfoAuth.type).toBe(MSG_TYPES.BROADCAST);
            expect(msgInfoAuth.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfoAuth.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
            expect(serverMessageAuth.message).toBe('broadcast message');
            expect(serverMessageAuth.authorName).toBe(MAIN_BUS_NAME);

            ws.close();
            wsAuth.close();
        });

        it('send()', async () => {
            const ws1 = new WebSocket(`ws://localhost:${PORT}`);
            const ws2 = new WebSocket(`ws://localhost:${PORT}`);

            // Need to wait for connection to save in local storage
            await sleep();

            { // Auth 1
                const serverAnswerPromise = awaitWsMessage(ws1);
                const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws1.send(msg);

                await serverAnswerPromise;
            }

            { // Auth 2
                const serverAnswerPromise = awaitWsMessage(ws2);
                const msg = formMsg([ 'User2', 'pass2' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws2.send(msg);

                await serverAnswerPromise;
            }

            let messagesTo1 = 0;
            let messagesTo2 = 0;

            ws1.on('message', () => ++messagesTo1);
            ws2.on('message', () => ++messagesTo2);

            const serverMessage1Promise = awaitWsMessage(ws1);
            const serverMessage2Promise = awaitWsMessage(ws2);

            wsServer.send(1, 'Message to 1');
            wsServer.send(2, 'Message to 2');

            const [ , , messageTo1 ] = JSON.parse(await serverMessage1Promise);
            const [ , , messageTo2 ] = JSON.parse(await serverMessage2Promise);

            expect(messageTo1.message).toBe('Message to 1');
            expect(messageTo1.authorName).toBe(MAIN_BUS_NAME);
            expect(messageTo2.message).toBe('Message to 2');
            expect(messageTo2.authorName).toBe(MAIN_BUS_NAME);
            expect(messagesTo1).toBe(1);
            expect(messagesTo2).toBe(1);

            ws1.removeAllListeners();
            ws2.removeAllListeners();

            ws1.close();
            ws2.close();
        });

        it('getLoggedUsers()', async () => {
            const ws1 = new WebSocket(`ws://localhost:${PORT}`);
            const ws2 = new WebSocket(`ws://localhost:${PORT}`);
            const wsNonAuth = new WebSocket(`ws://localhost:${PORT}`);

            // Need to wait for connection to save in local storage
            await sleep();

            { // Auth 1
                const serverAnswerPromise = awaitWsMessage(ws1);
                const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws1.send(msg);

                await serverAnswerPromise;
            }

            {
                const loggedUsers = wsServer.getLoggedUsers();

                expect(Object.keys(loggedUsers)).toHaveLength(1);
                expect(loggedUsers[1]).toBe('user1');
            }

            { // Auth 2
                const serverAnswerPromise = awaitWsMessage(ws2);
                const msg = formMsg([ 'User2', 'pass2' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws2.send(msg);

                await serverAnswerPromise;
            }

            {
                const loggedUsers = wsServer.getLoggedUsers();

                expect(Object.keys(loggedUsers)).toHaveLength(2);
                expect(loggedUsers[1]).toBe('user1');
                expect(loggedUsers[2]).toBe('user2');
            }

            ws1.close();

            // Need to wait for connection removing from local storage
            await sleep();

            {
                const loggedUsers = wsServer.getLoggedUsers();

                expect(Object.keys(loggedUsers)).toHaveLength(1);
                expect(loggedUsers[2]).toBe('user2');
            }

            ws2.close();

            // Need to wait for connection removing from local storage
            await sleep();

            {
                const loggedUsers = wsServer.getLoggedUsers();

                expect(Object.keys(loggedUsers)).toHaveLength(0);
            }

            wsNonAuth.close();
        });
    });

    describe('Clientside method calls', () => {
        it('broadcast()', async () => {
            const wsSender = new WebSocket(`ws://localhost:${PORT}`);
            const ws2 = new WebSocket(`ws://localhost:${PORT}`);
            const ws3 = new WebSocket(`ws://localhost:${PORT}`);
            const serverMessagePromise2 = awaitWsMessage(ws2);
            const serverMessagePromise3 = awaitWsMessage(ws3);

            // Need to wait for connection to save in local storage
            await sleep();

            { // Auth
                const serverAnswerPromise = awaitWsMessage(wsSender);
                const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                wsSender.send(msg);

                await serverAnswerPromise;
            }

            let timeoutErrorsCounter = 0;

            { // Calling broadcast
                const serverAnswerPromise = awaitWsMessage(wsSender)
                    .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });
                const msg = formMsg('broadcast message', { targetID: MAIN_BUS_ID, type: MSG_TYPES.BROADCAST });

                wsSender.send(msg);

                await serverAnswerPromise;
            }

            const [ msgTechInfo2, , serverMessage2 ] = JSON.parse(await serverMessagePromise2);
            const [ msgTechInfo3, , serverMessage3 ] = JSON.parse(await serverMessagePromise3);

            const msgInfo2 = getMsgInfo(msgTechInfo2);
            const msgInfo3 = getMsgInfo(msgTechInfo3);

            // Sender didn't receive self message
            expect(timeoutErrorsCounter).toBe(1);

            expect(msgInfo2.type).toBe(MSG_TYPES.BROADCAST);
            expect(msgInfo2.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfo2.flags, MSG_FLAGS.FROM_SERVER)).toBe(false);
            expect(serverMessage2.message).toBe('broadcast message');
            expect(serverMessage2.authorName).toBe('user1');

            expect(msgInfo3.type).toBe(MSG_TYPES.BROADCAST);
            expect(msgInfo3.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfo3.flags, MSG_FLAGS.FROM_SERVER)).toBe(false);
            expect(serverMessage3.message).toBe('broadcast message');
            expect(serverMessage3.authorName).toBe('user1');

            wsSender.close();
            ws2.close();
            ws3.close();
        });

        it('send()', async () => {
            const ws1 = new WebSocket(`ws://localhost:${PORT}`);
            const ws2 = new WebSocket(`ws://localhost:${PORT}`);
            const ws3 = new WebSocket(`ws://localhost:${PORT}`);

            // Need to wait for connection to save in local storage
            await sleep();

            { // Auth 1
                const serverAnswerPromise = awaitWsMessage(ws1);
                const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws1.send(msg);

                await serverAnswerPromise;
            }

            { // Auth 2
                const serverAnswerPromise = awaitWsMessage(ws2);
                const msg = formMsg([ 'User2', 'pass2' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws2.send(msg);

                await serverAnswerPromise;
            }

            { // Auth 3
                const serverAnswerPromise = awaitWsMessage(ws3);
                const msg = formMsg([ 'User3', 'pass3' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                ws3.send(msg);

                await serverAnswerPromise;
            }

            let timeoutErrorsCounter = 0;
            const clientMessagePromise1 = awaitWsMessage(ws1)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });
            const clientMessagePromise2 = awaitWsMessage(ws2)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });
            const clientMessagePromise3 = awaitWsMessage(ws3)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });

            { // Calling send
                const msg = formMsg('hello', { targetID: 2, type: MSG_TYPES.COMMON_MESSAGE });

                ws1.send(msg);
            }

            await clientMessagePromise1;
            const event2 = await clientMessagePromise2;
            await clientMessagePromise3;

            if (!event2) {
                throw new Error(`Not received event for auth user`);
            }

            const [ msgTechInfoAuth, , clientMessage ] = JSON.parse(event2);

            const msgInfoAuth = getMsgInfo(msgTechInfoAuth);

            expect(timeoutErrorsCounter).toBe(2);

            expect(msgInfoAuth.type).toBe(MSG_TYPES.COMMON_MESSAGE);
            expect(msgInfoAuth.dataType).toBe(DATA_TYPES.OBJECT);
            expect(_checkBit(msgInfoAuth.flags, MSG_FLAGS.FROM_SERVER)).toBe(false);
            expect(clientMessage.message).toBe('hello');
            expect(clientMessage.authorName).toBe('user1');

            ws1.close();
            ws2.close();
            ws3.close();
        });

        describe('rpc()', () => {
            it('registerRPC() and unregisterRPC()', async () => {
                const methodsAmountAtStart = wsServer.getRPCsList().length;

                function testRpc(params: Object) {
                    return params;
                }

                wsServer.registerRPC('method1', testRpc);

                {
                    const rpcsList = wsServer.getRPCsList();

                    expect(rpcsList).toHaveLength(methodsAmountAtStart + 1);
                    expect(rpcsList).toContain('method1');
                }

                wsServer.registerRPC('method2', testRpc);

                {
                    const rpcsList = wsServer.getRPCsList();

                    expect(rpcsList).toHaveLength(methodsAmountAtStart + 2);
                    expect(rpcsList).toContain('method1');
                    expect(rpcsList).toContain('method2');
                }

                wsServer.unregisterRPC('method2');

                {
                    const rpcsList = wsServer.getRPCsList();

                    expect(rpcsList).toHaveLength(methodsAmountAtStart + 1);
                    expect(rpcsList).toContain('method1');
                }

                wsServer.unregisterRPC('method1');

                {
                    const rpcsList = wsServer.getRPCsList();

                    expect(rpcsList).toHaveLength(methodsAmountAtStart);
                }
            });

            it('call undefined method', async () => {
                const ws1 = new WebSocket(`ws://localhost:${PORT}`);

                // Need to wait for connection to save in local storage
                await sleep();

                { // Auth 1
                    const serverAnswerPromise = awaitWsMessage(ws1);
                    const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                    ws1.send(msg);

                    await serverAnswerPromise;
                }

                const serverAnswerPromise1 = awaitWsMessage(ws1);

                // Calling rpc
                const rpcCallMessage: Message_RPC_CALL = {
                    uniqueID: '1',
                    methodName: 'unknownMethod',
                    params: { test: 1 },
                };
                const msg = formMsg(rpcCallMessage, { targetID: MAIN_BUS_ID, type: MSG_TYPES.RPC_CALL });

                ws1.send(msg);

                const event1 = await serverAnswerPromise1;

                if (!event1) {
                    throw new Error(`Not received event for auth user`);
                }

                const [ msgTechInfoAuth1, , rpcResponse ] = JSON.parse(event1);
                const msgInfo1 = getMsgInfo(msgTechInfoAuth1);

                if (!_isRPCResponseMessage(rpcResponse)) {
                    throw new Error(`Received not RPC response message`);
                }

                expect(msgInfo1.type).toBe(MSG_TYPES.RPC_RESPONSE);
                expect(msgInfo1.dataType).toBe(DATA_TYPES.OBJECT);
                expect(_checkBit(msgInfo1.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
                expect(rpcResponse.isSuccess).toBe(false);
                expect(rpcResponse.methodName).toBe(rpcCallMessage.methodName);
                expect(rpcResponse.uniqueID).toBe(rpcCallMessage.uniqueID);
                expect(rpcResponse.errorText).toBeDefined();
                expect(rpcResponse.result).toBeUndefined();
            });

            it('rpc throws error', async () => {
                function testRpc() {
                    throw new Error('RPC ERROR')
                }

                wsServer.registerRPC('method1', testRpc);
                const ws1 = new WebSocket(`ws://localhost:${PORT}`);

                // Need to wait for connection to save in local storage
                await sleep();

                { // Auth 1
                    const serverAnswerPromise = awaitWsMessage(ws1);
                    const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                    ws1.send(msg);

                    await serverAnswerPromise;
                }

                const serverAnswerPromise1 = awaitWsMessage(ws1);

                // Calling rpc
                const rpcCallMessage: Message_RPC_CALL = {
                    uniqueID: '1',
                    methodName: 'method1',
                    params: { test: 1 },
                };
                const msg = formMsg(rpcCallMessage, { targetID: MAIN_BUS_ID, type: MSG_TYPES.RPC_CALL });

                ws1.send(msg);

                const event1 = await serverAnswerPromise1;

                if (!event1) {
                    throw new Error(`Not received event for auth user`);
                }

                const [ msgTechInfoAuth1, , rpcResponse ] = JSON.parse(event1);
                const msgInfo1 = getMsgInfo(msgTechInfoAuth1);

                if (!_isRPCResponseMessage(rpcResponse)) {
                    throw new Error(`Received not RPC response message`);
                }

                expect(msgInfo1.type).toBe(MSG_TYPES.RPC_RESPONSE);
                expect(msgInfo1.dataType).toBe(DATA_TYPES.OBJECT);
                expect(_checkBit(msgInfo1.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
                expect(rpcResponse.isSuccess).toBe(false);
                expect(rpcResponse.methodName).toBe(rpcCallMessage.methodName);
                expect(rpcResponse.uniqueID).toBe(rpcCallMessage.uniqueID);
                expect(rpcResponse.errorText).toBe('RPC ERROR');
                expect(rpcResponse.result).toBeUndefined();
            });

            it('call defined method', async () => {
                function testRpc(params: Object) {
                    return params;
                }

                wsServer.registerRPC('method1', testRpc);

                const ws1 = new WebSocket(`ws://localhost:${PORT}`);

                // Need to wait for connection to save in local storage
                await sleep();

                { // Auth 1
                    const serverAnswerPromise = awaitWsMessage(ws1);
                    const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                    ws1.send(msg);

                    await serverAnswerPromise;
                }

                const serverAnswerPromise1 = awaitWsMessage(ws1);

                // Calling rpc
                const rpcParams = { test: 1 };
                const rpcCallMessage: Message_RPC_CALL = {
                    uniqueID: '1',
                    methodName: 'method1',
                    params: rpcParams,
                };
                const msg = formMsg(rpcCallMessage, { targetID: MAIN_BUS_ID, type: MSG_TYPES.RPC_CALL });

                ws1.send(msg);

                const event1 = await serverAnswerPromise1;

                if (!event1) {
                    throw new Error(`Not received event for auth user`);
                }

                const [ msgTechInfoAuth1, , rpcResponse ] = JSON.parse(event1);
                const msgInfo1 = getMsgInfo(msgTechInfoAuth1);

                if (!_isRPCResponseMessage(rpcResponse)) {
                    throw new Error(`Received not RPC response message`);
                }

                expect(msgInfo1.type).toBe(MSG_TYPES.RPC_RESPONSE);
                expect(msgInfo1.dataType).toBe(DATA_TYPES.OBJECT);
                expect(_checkBit(msgInfo1.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
                expect(rpcResponse.isSuccess).toBe(true);
                expect(rpcResponse.methodName).toBe(rpcCallMessage.methodName);
                expect(rpcResponse.uniqueID).toBe(rpcCallMessage.uniqueID);
                expect(rpcResponse.errorText).toBeUndefined();
                expect(rpcResponse.result).toEqual(rpcParams);

                ws1.close();
            });

            it('duplicate uniqueIDs', async () => {
                async function testRpc(params: Object) {
                    await sleep(100);

                    return params;
                }

                wsServer.registerRPC('method1', testRpc);

                const ws1 = new WebSocket(`ws://localhost:${PORT}`);
                const ws2 = new WebSocket(`ws://localhost:${PORT}`);

                // Need to wait for connection to save in local storage
                await sleep();

                { // Auth 1
                    const serverAnswerPromise = awaitWsMessage(ws1);
                    const msg = formMsg([ 'User1', 'pass1' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                    ws1.send(msg);

                    await serverAnswerPromise;
                }
                { // Auth 2
                    const serverAnswerPromise = awaitWsMessage(ws2);
                    const msg = formMsg([ 'User2', 'pass2' ], { targetID: MAIN_BUS_ID, type: MSG_TYPES.AUTH });

                    ws2.send(msg);

                    await serverAnswerPromise;
                }

                const serverAnswerPromise1 = awaitWsMessage(ws1);
                const serverAnswerPromise2 = awaitWsMessage(ws2);

                // Calling rpc
                const rpcParams1 = { test: 1 };
                const rpcParams2 = { test: 2 };
                const rpcCallMessage1: Message_RPC_CALL = {
                    uniqueID: '1',
                    methodName: 'method1',
                    params: rpcParams1,
                };
                const rpcCallMessage2: Message_RPC_CALL = {
                    uniqueID: '1',
                    methodName: 'method1',
                    params: rpcParams2,
                };
                const msg1 = formMsg(rpcCallMessage1, { targetID: MAIN_BUS_ID, type: MSG_TYPES.RPC_CALL });
                const msg2 = formMsg(rpcCallMessage2, { targetID: MAIN_BUS_ID, type: MSG_TYPES.RPC_CALL });

                ws1.send(msg1);
                ws2.send(msg2);

                const [
                    event1,
                    event2,
                ] = await Promise.all([
                    serverAnswerPromise1,
                    serverAnswerPromise2,
                ])

                if (!event1) {
                    throw new Error(`Not received event for auth user`);
                }
                if (!event2) {
                    throw new Error(`Not received event for auth user`);
                }

                const [ msgTechInfoAuth1, , rpcResponse1 ] = JSON.parse(event1);
                const [ msgTechInfoAuth2, , rpcResponse2 ] = JSON.parse(event2);
                const msgInfo1 = getMsgInfo(msgTechInfoAuth1);
                const msgInfo2 = getMsgInfo(msgTechInfoAuth2);

                if (!_isRPCResponseMessage(rpcResponse1)) {
                    throw new Error(`Received not RPC response message`);
                }
                if (!_isRPCResponseMessage(rpcResponse2)) {
                    throw new Error(`Received not RPC response message`);
                }

                expect(msgInfo1.type).toBe(MSG_TYPES.RPC_RESPONSE);
                expect(msgInfo1.dataType).toBe(DATA_TYPES.OBJECT);
                expect(_checkBit(msgInfo1.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
                expect(rpcResponse1.isSuccess).toBe(true);
                expect(rpcResponse1.methodName).toBe(rpcCallMessage1.methodName);
                expect(rpcResponse1.uniqueID).toBe(rpcCallMessage1.uniqueID);
                expect(rpcResponse1.errorText).toBeUndefined();
                expect(rpcResponse1.result).toEqual(rpcParams1);

                expect(msgInfo2.type).toBe(MSG_TYPES.RPC_RESPONSE);
                expect(msgInfo2.dataType).toBe(DATA_TYPES.OBJECT);
                expect(_checkBit(msgInfo2.flags, MSG_FLAGS.FROM_SERVER)).toBe(true);
                expect(rpcResponse2.isSuccess).toBe(true);
                expect(rpcResponse2.methodName).toBe(rpcCallMessage2.methodName);
                expect(rpcResponse2.uniqueID).toBe(rpcCallMessage2.uniqueID);
                expect(rpcResponse2.errorText).toBeUndefined();
                expect(rpcResponse2.result).toEqual(rpcParams2);

                ws1.close();
                ws2.close();
            });
        });
    });
});

async function sleep(ms = 300) {
    await new Promise((resolve) => setTimeout(resolve, ms));
}

const TEST_TIMEOUT_ERROR = 'Test: Timeout';

async function awaitWsMessage(ws: WebSocket, timeoutMs = 500): Promise<string> {
    return new Promise((resolve, reject) => {
        const timeout = setTimeout(() => reject(TEST_TIMEOUT_ERROR), timeoutMs);

        ws.once('message', (data) => {
            clearTimeout(timeout);

            const msg = String(data);

            resolve(msg);
        });
    });
}

function _checkBit(mask: number, bit: number): boolean {
    return (mask & bit) !== 0;
}