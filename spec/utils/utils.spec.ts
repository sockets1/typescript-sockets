
import {
    genMsgInfo,
    getMsgInfo,
} from "../../utils/common";
import {
    DATA_TYPES,
    MSG_TYPES,
} from "../../types/messages";
import {
    MSG_FLAGS,
} from "../../utils/flags";
import type { IWebSocket } from "../../types/types";

describe('utils', () => {
    describe('msgInfo', () => {
        it('case 1', () => {
            const info: IWebSocket.IncomingMessageInfo = {
                type: MSG_TYPES.BROADCAST,
                dataType: DATA_TYPES.STRING,
                flags: MSG_FLAGS.BASE | MSG_FLAGS.FROM_SERVER,
            };

            const genInfo = genMsgInfo(
                info.type,
                info.dataType,
                info.flags,
            );

            const getInfo = getMsgInfo(genInfo);

            expect(getInfo).toEqual(info);
        });

        it('case 2', () => {
            const info: IWebSocket.IncomingMessageInfo = {
                type: MSG_TYPES.AUTH,
                dataType: DATA_TYPES.STRING,
                flags: MSG_FLAGS.FROM_SERVER,
            };

            const genInfo = genMsgInfo(
                info.type,
                info.dataType,
                info.flags,
            );

            const getInfo = getMsgInfo(genInfo);

            expect(getInfo).toEqual(info);
        });

        it('case 3', () => {
            const info: IWebSocket.IncomingMessageInfo = {
                type: MSG_TYPES.COMMON_MESSAGE,
                dataType: DATA_TYPES.ARRAY_BUFFER,
                flags: MSG_FLAGS.PRIOR | MSG_FLAGS.WARNING | MSG_FLAGS.FROM_SERVER,
            };

            const genInfo = genMsgInfo(
                info.type,
                info.dataType,
                info.flags,
            );

            const getInfo = getMsgInfo(genInfo);

            expect(getInfo).toEqual(info);
        });
    });
});