import WSServerLocal from "../../WSServer/WSServerLocal";
import WSClient from "../../WSClient/WSClient";
import {BASE_EVENTS} from "../../types/events";
import {Message_COMMON_MESSAGE} from "../../types/messages";
import {RPC_NAME__registerRPC, RPC_NAME__unregisterRPC} from "../../utils/consts";

const PORT = 8080;
let wsServer: WSServerLocal;

describe('WSClient', () => {
    beforeAll(async () => {
        wsServer = new WSServerLocal({ port: PORT });

        wsServer.initUserData([
            {
                id: 1,
                username: 'user1',
                password: 'pass1',
            },
            {
                id: 2,
                username: 'user2',
                password: 'pass2',
            },
            {
                id: 3,
                username: 'user3',
                password: 'pass3',
            },
        ]);

        await wsServer.start();
    });

    afterAll(async () => {
        wsServer.destructor();
    });

    afterEach(async () => {
        await sleep();

        const rpcsList = wsServer.getRPCsList();

        for (const rpcName of rpcsList) {
            if (rpcName !== RPC_NAME__registerRPC && rpcName !== RPC_NAME__unregisterRPC) {
                wsServer.unregisterRPC(rpcName);
            }
        }
    });

    describe('Methods', () => {
        describe('auth()', () => {
            it('Correct credentials', async () => {
                const ws = new WSClient();

                await ws.connect(`ws://localhost:${PORT}`);

                await ws.auth({
                    username: 'user1',
                    password: 'pass1',
                });

                expect(ws.isAuth).toBeTruthy();
                expect(ws.username).toBe('user1');
                expect(ws.id).toBe(1);

                ws.destructor();
            });

            it('Incorrect credentials', async () => {
                const ws = new WSClient();

                await ws.connect(`ws://localhost:${PORT}`);

                let error: Error | void = void 0;

                await ws.auth({
                    username: 'user1',
                    password: 'pass2',
                }).catch(err => {
                    error = err;
                });

                expect(error).toBeDefined();
                expect((error as Error).message).toBe('Incorrect username or password');
                expect(ws.isAuth).toBeFalsy();
                expect(ws.username).toBeUndefined();
                expect(ws.id).toBeUndefined();

                ws.destructor();
            });
        });

        it('send()', async () => {
            const wsSender = new WSClient();
            const wsReceiver = new WSClient();
            const wsJustIdle = new WSClient();

            await wsSender.connect(`ws://localhost:${PORT}`);
            await wsReceiver.connect(`ws://localhost:${PORT}`);
            await wsJustIdle.connect(`ws://localhost:${PORT}`);

            await wsSender.auth({
                username: 'user3',
                password: 'pass3',
            });
            await wsReceiver.auth({
                username: 'user1',
                password: 'pass1',
            });
            await wsJustIdle.auth({
                username: 'user2',
                password: 'pass2',
            });

            let timeoutErrorsCounter = 0;
            const messagePromiseReceiver = awaitWsMessage<Message_COMMON_MESSAGE>(wsReceiver, BASE_EVENTS.MESSAGE)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });
            const messagePromiseIdle = awaitWsMessage<Message_COMMON_MESSAGE>(wsJustIdle, BASE_EVENTS.MESSAGE)
                .catch((err) => { err === TEST_TIMEOUT_ERROR ? ++timeoutErrorsCounter : void 0 });

            wsSender.send('Hello everybody', wsReceiver.id);

            const messageEventReceiver = await messagePromiseReceiver;
            const messageEventIdle = await messagePromiseIdle;

            if (!messageEventReceiver) {
                throw new Error(`Not received event`);
            }

            expect(timeoutErrorsCounter).toBe(1);
            expect(messageEventIdle).toBeUndefined();
            expect(messageEventReceiver.message).toBe('Hello everybody');
            expect(messageEventReceiver.authorId).toBe(wsSender.id);
            expect(messageEventReceiver.authorName).toBe(wsSender.username);

            wsSender.destructor();
            wsReceiver.destructor();
            wsJustIdle.destructor();
        });


        it('broadcast()', async () => {
            const wsSender = new WSClient();
            const ws1 = new WSClient();
            const ws2 = new WSClient();

            await wsSender.connect(`ws://localhost:${PORT}`);
            await ws1.connect(`ws://localhost:${PORT}`);
            await ws2.connect(`ws://localhost:${PORT}`);

            await wsSender.auth({
                username: 'user3',
                password: 'pass3',
            });
            await ws1.auth({
                username: 'user1',
                password: 'pass1',
            });
            await ws2.auth({
                username: 'user2',
                password: 'pass2',
            });

            const messagePromise1 = awaitWsMessage<Message_COMMON_MESSAGE>(ws1, BASE_EVENTS.MESSAGE);
            const messagePromise2 = awaitWsMessage<Message_COMMON_MESSAGE>(ws2, BASE_EVENTS.MESSAGE);

            wsSender.broadcast('Hello everybody');

            const messageEvent1 = await messagePromise1;
            const messageEvent2 = await messagePromise2;

            expect(messageEvent1.message).toBe('Hello everybody');
            expect(messageEvent1.authorId).toBe(wsSender.id);
            expect(messageEvent1.authorName).toBe(wsSender.username);
            expect(messageEvent2.message).toBe('Hello everybody');
            expect(messageEvent2.authorId).toBe(wsSender.id);
            expect(messageEvent2.authorName).toBe(wsSender.username);

            wsSender.destructor();
            ws1.destructor();
            ws2.destructor();
        });

        describe('rpc', () => {
            it('server rpc', async () => {
                wsServer.registerRPC('backend:getLoggedUsers', wsServer.getLoggedUsers.bind(wsServer));

                const ws1 = new WSClient();
                const ws2 = new WSClient();

                await ws1.connect(`ws://localhost:${PORT}`);
                await ws2.connect(`ws://localhost:${PORT}`);

                await ws1.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await ws2.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                const serverMethodResult = wsServer.getLoggedUsers();
                const rpcMethodResult = await ws1.call('backend:getLoggedUsers');

                expect(rpcMethodResult).toEqual(serverMethodResult);

                ws1.destructor();
                ws2.destructor();
            });

            it('client rpc, success call', async () => {
                const wsRPC = new WSClient();
                const wsCaller = new WSClient();

                await wsRPC.connect(`ws://localhost:${PORT}`);
                await wsCaller.connect(`ws://localhost:${PORT}`);

                await wsRPC.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await wsCaller.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                async function testRPC(params: Record<string, unknown>) {
                    return params;
                }

                await wsRPC.registerRPC('method1', testRPC);

                expect(wsServer.getRPCsList()).toContain('method1');

                const rpcParams = { test: 1 };
                const rpcMethodResult = await wsCaller.call('method1', rpcParams);
                const expectResult = await testRPC(rpcParams);

                expect(rpcMethodResult).toEqual(expectResult);

                wsRPC.destructor();
                wsCaller.destructor();
            });

            it('client rpc, error call', async () => {
                const wsRPC = new WSClient();
                const wsCaller = new WSClient();

                await wsRPC.connect(`ws://localhost:${PORT}`);
                await wsCaller.connect(`ws://localhost:${PORT}`);

                await wsRPC.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await wsCaller.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                async function testRPC(params: Record<string, unknown>) {
                    throw new Error(`RPC Error`);
                }

                await wsRPC.registerRPC('method1', testRPC);

                expect(wsServer.getRPCsList()).toContain('method1');

                let rpcError = '';

                await wsCaller.call('method1', {}).catch(err => rpcError = err.message);

                expect(rpcError).toBe('RPC Error');

                wsRPC.destructor();
                wsCaller.destructor();
            });

            it('local prevent unregistering another owners rpc', async () => {
                const wsRPC = new WSClient();
                const wsHacker = new WSClient();

                await wsRPC.connect(`ws://localhost:${PORT}`);
                await wsHacker.connect(`ws://localhost:${PORT}`);

                await wsRPC.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await wsHacker.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                async function testRPC(params: Record<string, unknown>) {
                    throw new Error(`RPC Error`);
                }

                await wsRPC.registerRPC('method1', testRPC);

                expect(wsServer.getRPCsList()).toContain('method1');

                await wsHacker.unregisterRPC('method1');

                expect(wsServer.getRPCsList()).toContain('method1');

                wsRPC.destructor();
                wsHacker.destructor();
            });
        });

        describe('events', () => {
            it('server.broadcastEvent() without tag', async () => {
                const ws1 = new WSClient();
                const ws2 = new WSClient();
                const ws3 = new WSClient();

                await ws1.connect(`ws://localhost:${PORT}`);
                await ws2.connect(`ws://localhost:${PORT}`);
                await ws3.connect(`ws://localhost:${PORT}`);

                await ws1.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await ws2.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                const eventName = 'customEvent';
                const eventData = { testData: 1 };

                const eventPromise1 = awaitWsMessage(ws1, eventName);
                const eventPromise2 = awaitWsMessage(ws2, eventName);
                const eventPromise3 = awaitWsMessage(ws3, eventName);

                await wsServer.broadcastEvent(eventName, eventData);

                const event1 = await eventPromise1 as (typeof eventData);
                const event2 = await eventPromise2 as (typeof eventData);
                const notReceivedEvent = await eventPromise3.catch(() => void 0);

                expect(event1).toEqual(eventData);
                expect(event2).toEqual(eventData);
                expect(notReceivedEvent).toBe(void 0);

                ws1.destructor();
                ws2.destructor();
                ws3.destructor();
            });

            it('server.broadcastEvent() with tag', async () => {
                const ws1 = new WSClient();
                const ws2 = new WSClient();
                const ws3 = new WSClient();

                await ws1.connect(`ws://localhost:${PORT}`);
                await ws2.connect(`ws://localhost:${PORT}`);
                await ws3.connect(`ws://localhost:${PORT}`);

                await ws1.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await ws2.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                const eventName = 'customEvent';
                const eventTag = 'tag1';
                const eventData = { testData: 1 };

                const eventPromise1 = awaitWsMessage(ws1, eventName);
                const eventPromise2WithTag = awaitWsMessage(ws2, `${eventName}.${eventTag}`);
                const eventPromise2WithoutTag = awaitWsMessage(ws2, eventName);
                const eventPromise3 = awaitWsMessage(ws3, eventName);

                await wsServer.broadcastEvent(eventName, eventData, void 0, eventTag);

                const event1 = await eventPromise1 as (typeof eventData);
                const event2WithTag = await eventPromise2WithTag as (typeof eventData);
                const event2WithoutTag = await eventPromise2WithoutTag as (typeof eventData);
                const notReceivedEvent = await eventPromise3.catch(() => void 0);

                expect(event1).toEqual(eventData);
                expect(event2WithTag).toEqual(eventData);
                expect(event2WithoutTag).toEqual(eventData);
                expect(notReceivedEvent).toBe(void 0);

                ws1.destructor();
                ws2.destructor();
                ws3.destructor();
            });

            it('sendEvent() without tag', async () => {
                const wsSender = new WSClient();
                const wsReceiver = new WSClient();
                const wsNoAuth = new WSClient();

                await wsSender.connect(`ws://localhost:${PORT}`);
                await wsReceiver.connect(`ws://localhost:${PORT}`);
                await wsNoAuth.connect(`ws://localhost:${PORT}`);

                await wsSender.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await wsReceiver.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                const eventName = 'customEvent';
                const nonExistingTag = 'tag1';
                const eventData = { testData: 1 };

                const eventPromise2WithTag = awaitWsMessage(wsReceiver, `${eventName}.${nonExistingTag}`);
                const eventPromise2WithoutTag = awaitWsMessage(wsReceiver, eventName);
                const eventPromise3 = awaitWsMessage(wsNoAuth, eventName);

                wsSender.sendEvent(wsReceiver.id, eventName, eventData);

                const event2WithTag = await eventPromise2WithTag.catch(() => void 0);
                const event2WithoutTag = await eventPromise2WithoutTag as (typeof eventData);
                const notReceivedEvent = await eventPromise3.catch(() => void 0);

                expect(event2WithTag).toBe(void 0);
                expect(event2WithoutTag).toEqual(eventData);
                expect(notReceivedEvent).toBe(void 0);

                wsSender.destructor();
                wsReceiver.destructor();
                wsNoAuth.destructor();
            });

            it('sendEvent() with tag', async () => {
                const wsSender = new WSClient();
                const wsReceiver = new WSClient();
                const wsNoAuth = new WSClient();

                await wsSender.connect(`ws://localhost:${PORT}`);
                await wsReceiver.connect(`ws://localhost:${PORT}`);
                await wsNoAuth.connect(`ws://localhost:${PORT}`);

                await wsSender.auth({
                    username: 'user1',
                    password: 'pass1',
                });
                await wsReceiver.auth({
                    username: 'user2',
                    password: 'pass2',
                });

                const eventName = 'customEvent';
                const eventTag = 'tag1';
                const eventData = { testData: 1 };

                const eventPromise2WithTag = awaitWsMessage(wsReceiver, `${eventName}.${eventTag}`);
                const eventPromise2WithoutTag = awaitWsMessage(wsReceiver, eventName);
                const eventPromise3 = awaitWsMessage(wsNoAuth, eventName);

                wsSender.sendEvent(wsReceiver.id, eventName, eventData, eventTag);

                const event2WithTag = await eventPromise2WithTag as (typeof eventData);
                const event2WithoutTag = await eventPromise2WithoutTag as (typeof eventData);
                const notReceivedEvent = await eventPromise3.catch(() => void 0);

                expect(event2WithTag).toEqual(eventData);
                expect(event2WithoutTag).toEqual(eventData);
                expect(notReceivedEvent).toBe(void 0);

                wsSender.destructor();
                wsReceiver.destructor();
                wsNoAuth.destructor();
            });
        });
    });
});

const TEST_TIMEOUT_ERROR = 'Test: Timeout';

async function awaitWsMessage<T = unknown>(ws: WSClient, eventType: BASE_EVENTS | string, timeoutMs = 500): Promise<T> {
    return new Promise((resolve, reject) => {
        const timeout = setTimeout(() => reject(TEST_TIMEOUT_ERROR), timeoutMs);

        ws.once(eventType, (data) => {
            clearTimeout(timeout);

            resolve(data);
        });
    });
}

async function sleep(ms = 300) {
    await new Promise((resolve) => setTimeout(resolve, ms));
}