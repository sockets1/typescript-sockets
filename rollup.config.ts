
import type { RollupOptions, Plugin } from "rollup";
import fs from "fs";
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';

interface PluginsConfig {
    compressLvl?: number;
}
function getPlugins(config: PluginsConfig = {}): Plugin[] {
    const {
        compressLvl = 1,
    } = config;

    const plugins = [
        typescript(),
    ];

    if (compressLvl) {
        plugins.push(terser());
    }

    return plugins;
}

const nodeExternals = [
    'buffer',
    'ws',
    'events',
    'node:http',
    'http',
];

function formWSServerInputs(config: PluginsConfig = {}): RollupOptions[] {
    const files = fs.readdirSync('./WSServer').filter(filename => filename !== 'WSServer.ts');
    const inputs: RollupOptions[] = [];

    for (const filename of files) {
        inputs.push({
            input: `./WSServer/${filename}`,
            output: {
                file: `output/${filename.replace('.ts', '.js')}`,
                format: 'cjs',
                sourcemap: false,
            },
            external: nodeExternals,
            plugins: getPlugins(config),
        })
    }

    return inputs;
}

const bundle: RollupOptions[] = [
    {
        input: "./WSClient/WSClient.ts",
        output: {
            file: "output/WSClient.js",
            format: 'cjs',
            sourcemap: false,
        },
        external: nodeExternals,
        plugins: getPlugins(),
    },
    ...formWSServerInputs(),
];

export default bundle