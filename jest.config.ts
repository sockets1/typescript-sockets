
import tsconfig from './tsconfig.json';
import type {
    JestConfigWithTsJest,
    TsJestTransformerOptions,
} from "ts-jest";

const {
    compilerOptions,
} = tsconfig;

delete compilerOptions.outDir;

compilerOptions.target = 'es2020';
compilerOptions.sourceMap = true;
compilerOptions.removeComments = false;
compilerOptions.preserveConstEnums = true;

const tsJest_globals: TsJestTransformerOptions = {
    compiler: 'typescript',
    diagnostics: false,
    tsconfig: compilerOptions,
};

const config: JestConfigWithTsJest = {
    preset: "ts-jest",
    testEnvironment: 'node',
    verbose: true,
    testMatch: [ '<rootDir>/spec/**/*[_\.](spec|test|snap).[jt]s?(x)' ],
    modulePathIgnorePatterns: [
        '<rootDir>/build/',
        '<rootDir>/build_cache/',
        '/node_modules/',
    ],
    testPathIgnorePatterns: [
        '/node_modules/',
        '<rootDir>/build/',
        '<rootDir>/build_cache/',
        '<rootDir>/spec_utils/',
        '<rootDir>/spec_data/',
    ],
    coveragePathIgnorePatterns: [
        '/node_modules/',
        '<rootDir>/build/',
        '<rootDir>/build_cache/',
        '<rootDir>/spec_utils/',
        '<rootDir>/spec_data/',
    ],

    transform: {
        '[\\[\\]?*+|{}\\\\()@.\n\r]': [ 'ts-jest', tsJest_globals ],
    },
};

export default config;