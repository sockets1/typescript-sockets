
import {_assert_isWS, _isRPCResponseMessage} from "./guards";
import {IWebSocket} from "../types/types";
import {getMsgInfo} from "./common";
import {Message_RPC_RESPONSE, MSG_TYPES} from "../types/messages";
import {handleEvent} from "./handleEvent";
import type { WebSocketServer } from "ws";

export function awaitRPCResponse<T = unknown>(
    currentConnection: IWebSocket.UnknownConnection | IWebSocket.Connection | WebSocketServer | void,
    methodName: string,
    uniqueID: string,
    logger: IWebSocket.Logger = console,
    timeoutMs = 10000,
): Promise<Message_RPC_RESPONSE<T>> {
    _assert_isWS(currentConnection);

    const promise = new Promise<T>((resolve, reject) => {
        const listener = (eventBuffer: IWebSocket.RawData) => {
            const [ msgTechInfo, /* targetId */, rpcResponse ] = handleEvent(eventBuffer, logger);

            const { type: msgType } = getMsgInfo(msgTechInfo);

            if (msgType !== MSG_TYPES.RPC_RESPONSE) {
                return;
            }

            if (!_isRPCResponseMessage(rpcResponse)) {
                logger.error('Received invalid RPC response', JSON.stringify(rpcResponse));

                return reject(`Invalid RPC response ${JSON.stringify(rpcResponse)}`);
            }
            else if (rpcResponse.uniqueID !== uniqueID) {
                return;
            }
            else {
                clearTimeout(timeout);
                currentConnection.off('message', listener);

                const {
                    result,
                    isSuccess,
                    errorText,
                } = rpcResponse;

                if (isSuccess) {
                    return resolve(result as T);
                }
                else {
                    return reject(errorText);
                }
            }
        }

        const timeout = setTimeout(() => {
            currentConnection.off('message', listener);
            reject('Timeout');
        }, timeoutMs);

        currentConnection.on('message', listener);
    });

    return promise
        .then(result => {
            return {
                uniqueID,
                methodName,
                isSuccess: true,
                result: result as T,
            } as Message_RPC_RESPONSE<T>;
        })
        .catch(err => {
            return {
                uniqueID,
                methodName,
                isSuccess: false,
                errorText: String(err),
            } as Message_RPC_RESPONSE<T>;
        })
    ;
}