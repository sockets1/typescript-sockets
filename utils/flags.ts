
/**
 * Flags for messages.
 * Main thing: this values could be combined
 *
 * Max: 21 (31 - 5 for MSG_TYPES - 5 for DATA_TYPES)
 */

export interface IFlags {
    /** Default */
    BASE?: boolean,
    /** In priority comparing with base */
    PRIOR?: boolean,
    /** Warning message */
    WARNING?: boolean,
    /** Message from server */
    FROM_SERVER?: boolean,
}

export const enum MSG_FLAGS {
    // Default
    BASE = 1 << 0,
    // In priority comparing with base
    PRIOR = 1 << 1,
    // Warning message
    WARNING = 1 << 2,
    // Message from server
    FROM_SERVER = 1 << 3,
}

/**
 * Method to transform human-readable flags to single number
 *
 * @param flagsObj map of flags
 * @returns flags as number
 */
export function genMsgFlag(flagsObj: IFlags = { BASE: true }): number {
    let flags = 0;

    if (flagsObj.BASE)
        flags |= MSG_FLAGS.BASE;
    if (flagsObj.PRIOR)
        flags |= MSG_FLAGS.PRIOR;
    if (flagsObj.WARNING)
        flags |= MSG_FLAGS.WARNING;
    if (flagsObj.FROM_SERVER)
        flags |= MSG_FLAGS.FROM_SERVER;

    return flags;
}