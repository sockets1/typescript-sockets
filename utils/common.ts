
import type { IWebSocket } from "../types/types";
import {
    genMsgFlag,
    IFlags
} from "./flags";
import {
    DATA_TYPES,
    MSG_FLAGS,
    MSG_TYPES,
} from "../types/messages";
import { Blob } from 'buffer'

export function formMsg(data: IWebSocket.ValidMessageData, options: IWebSocket.FormMessageOptions): string {
    const {
        type,
        flags,
        targetID,
    } = options;
    let flagsNormalized: number = MSG_FLAGS.BASE;
    let normalDataType: DATA_TYPES = getDataType(data);

    // Building flags as number
    if (flags) {
        if (typeof flags === 'number') {
            flagsNormalized = flags;
        }
        else if (typeof flags === 'object') {
            flagsNormalized = genMsgFlag(flags);
        }
        else {
            throw new Error(`[formMsg] Incorrect flags formed`);
        }
    }

    const msgTechInfo = genMsgInfo(type, normalDataType, flagsNormalized);

    return JSON.stringify([ msgTechInfo, targetID, data ] as IWebSocket.OutgoingMessage);
}

const bitsForType = 5;
const maskForType = (1 << bitsForType) - 1;
const bitsForDataType = 5;
const maskForDataType = ((1 << bitsForDataType) - 1) << bitsForType;

/**
 * This method formats all tech info about message into single number
 *
 * @returns Int32 with all message tech info
 *  |21 bits|5 bits|5 bits| <-> |flags|datatype|type|
 */
export function genMsgInfo(type: MSG_TYPES, dataType: DATA_TYPES, flags: IFlags | number): number {
    const preparedFlags = (typeof flags === "number") ? flags : genMsgFlag(flags);

    return (type) | (dataType << bitsForType) | (preparedFlags << (bitsForType + bitsForDataType));
}

export function getMsgInfo(msgTechInfo: number): IWebSocket.IncomingMessageInfo {
    return {
        type: (msgTechInfo & maskForType) as MSG_TYPES,
        dataType: ((msgTechInfo & maskForDataType) >> 5) as DATA_TYPES,
        flags: (msgTechInfo >> 10) as MSG_FLAGS,
    };
}

/**
 * Method to transform flags as number to human-readable flags map
 *
 * @param flags flags as number
 * @returns human-readable map of flags
 */
export function getMsgFlags(flags: number): IFlags {
    const flagsObj: IFlags = {};

    if (_checkBit(flags, MSG_FLAGS.BASE))
        flagsObj.BASE = true;
    if (_checkBit(flags, MSG_FLAGS.PRIOR))
        flagsObj.PRIOR = true;
    if (_checkBit(flags, MSG_FLAGS.WARNING))
        flagsObj.WARNING = true;
    if (_checkBit(flags, MSG_FLAGS.FROM_SERVER))
        flagsObj.FROM_SERVER = true;

    return flagsObj;
}

export function getDataType(data: unknown): DATA_TYPES {
    // Handling all available types of socket data
    if (typeof Blob !== void 0 && data instanceof Blob) {
        return DATA_TYPES.BLOB;
    }
    else if (data instanceof ArrayBuffer) {
        return DATA_TYPES.ARRAY_BUFFER;
    }
    // TypedArray
    else if (
        data instanceof Int8Array || data instanceof Uint8Array || data instanceof Uint8ClampedArray
        || data instanceof Int16Array || data instanceof Int32Array || data instanceof Float32Array
        || data instanceof Uint16Array || data instanceof Uint32Array || data instanceof Float64Array
        || data instanceof BigInt64Array || data instanceof BigUint64Array
    ) {
        return DATA_TYPES.NUMBER_ARRAY;
    }
    else if (Array.isArray(data)) {
        return DATA_TYPES.ARRAY;
    }
    else if (typeof data === "string") {
        return DATA_TYPES.STRING;
    }
    else if (typeof data === "object") {
        return DATA_TYPES.OBJECT;
    }
    else {
        console.error('[getDataType] Error while trying to detect data.\n', data);

        return DATA_TYPES.UNKNOWN;
    }
}

export function genUniqueID(): string {
    return Math.ceil(Math.random() * 9e9).toString(36);
}

function _checkBit(mask: number, bit: number): boolean {
    return (mask & bit) !== 0;
}

/*
function _unsetBit(mask: number, bit: number): number {
    return mask & ~bit;
}
*/
