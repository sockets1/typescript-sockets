
import type { IWebSocket } from "../types/types";

export function handleEvent(eventBuffer: IWebSocket.RawData, logger: IWebSocket.Logger = console) {
    if (!(eventBuffer instanceof Buffer)) {
        logger.error('Received message of unknown format', typeof eventBuffer);

        return;
    }
    const event = eventBuffer.toString();

    let message: IWebSocket.IncomingMessage | void = void 0;

    try {
        message = JSON.parse(event);
    }
    catch (err) {
        logger.error(`Received non-json auth message: ${event}\n`, err);
    }

    if (!message) {
        return;
    }

    if (!Array.isArray(message)) {
        logger.error(`Received incorrect message: ${event}`);

        return;
    }

    return message;
}