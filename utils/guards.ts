
import WebSocket, { WebSocketServer } from 'ws';
import {
    Message_AUTH,
    Message_COMMON_MESSAGE, Message_EVENT,
    Message_RPC_CALL,
    Message_RPC_RESPONSE,
} from "../types/messages";
import { IWebSocket } from "../types/types";

export function _assert_isWS(item?: unknown): asserts item is WebSocket {
    if (!item || (
        !(item instanceof WebSocket) && !(item instanceof WebSocketServer)
        )
    ) {
        throw new Error('Connection is not defined');
    }
}

export function _assert_isWSAuth(item?: unknown): asserts item is IWebSocket.Connection {
    if (
        !item
        || !(item instanceof WebSocket)
        || !(item as unknown as IWebSocket.Connection).id
        || typeof (item as unknown as IWebSocket.Connection).user !== 'object'
        || !(item as unknown as IWebSocket.Connection).user.username
        || !(item as unknown as IWebSocket.Connection).user.id
    ) {
        throw new Error('Connection is not authorized');
    }
}

export function _assert_isAuth(isAuth: boolean): asserts isAuth is true {
    if (!isAuth) {
        throw new Error('Not authorized');
    }
}

export function _isAuthRequestMessage(message?: Object | void): message is [ username: string, password: string ] {
    return message
        && typeof message === 'object'
        && Array.isArray(message)
        && message.length === 2
        && typeof message[0] === 'string'
        && typeof message[1] === 'string'
    ;
}

export function _isAuthMessage(message?: Object | void): message is Message_AUTH {
    return !!(message
        && typeof message === 'object'
        && typeof (message as Message_AUTH).message === 'string'
        && ((
            (message as Message_AUTH).isSuccess === true
            && typeof (message as Message_AUTH).userData === 'object'
            && typeof (message as Message_AUTH).message === 'string'
            ) || (
            (message as Message_AUTH).isSuccess === false
            && !(message as Message_AUTH).userData
        ))
    );
}

export function _isRPCCallMessage(message?: Object | void): message is Message_RPC_CALL {
    return message
        && typeof message === 'object'
        && typeof (message as Message_RPC_CALL).methodName === 'string'
        && (
            typeof (message as Message_RPC_CALL).uniqueID === 'string'
            || typeof (message as Message_RPC_CALL).uniqueID === 'number'
        )
    ;
}

export function _isRPCResponseMessage(message?: Object | void): message is Message_RPC_RESPONSE {
    return message
        && typeof message === 'object'
        && typeof (message as Message_RPC_RESPONSE).methodName === 'string'
        && typeof (message as Message_RPC_RESPONSE).isSuccess === 'boolean'
        && (
            typeof (message as Message_RPC_RESPONSE).uniqueID === 'string'
            || typeof (message as Message_RPC_RESPONSE).uniqueID === 'number'
        )
    ;
}

export function _isCommonMessage(message?: Object | void): message is Message_COMMON_MESSAGE {
    return message
        && typeof message === 'object'
        && typeof (message as Message_COMMON_MESSAGE).authorId === 'number'
        && typeof (message as Message_COMMON_MESSAGE).authorName === 'string'
        && !!(message as Message_COMMON_MESSAGE).message
    ;
}

export function _isEventMessage(message?: Object | void): message is Message_EVENT {
    return message
        && typeof message === 'object'
        && (typeof (message as Message_EVENT).event === 'number' || typeof (message as Message_EVENT).event === 'string')
        && !!(message as Message_EVENT).data
    ;
}