
export const MAIN_BUS_ID = -1;
export const MAIN_BUS_NAME = 'MAIN_BUS';
export const UNKNOWN_ID = -3;
export const UNKNOWN_NAME = 'unknown user';
export const AUTH_MESSAGE_TEMPLATE = 'Successfully authorized';

export const RPC_NAME__getLoggedUsers = 'getLoggedUsers';
export const RPC_NAME__registerRPC = 'registerRPC';
export const RPC_NAME__unregisterRPC = 'unregisterRPC';