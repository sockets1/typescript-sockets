import type {IWebSocket} from "../types/types";
// import type {CloseEvent, ErrorEvent} from 'ws';
import WebSocket from "ws";
import {MAIN_BUS_ID, RPC_NAME__getLoggedUsers, RPC_NAME__registerRPC, RPC_NAME__unregisterRPC} from "../utils/consts";
import {BASE_EVENTS} from "../types/events";
import {formMsg, genUniqueID, getMsgInfo} from "../utils/common";
import {
    _assert_isAuth,
    _assert_isWS,
    _isAuthMessage,
    _isCommonMessage,
    _isEventMessage,
    _isRPCCallMessage
} from "../utils/guards";
import {Message_AUTH, Message_EVENT, Message_RPC_CALL, Message_RPC_RESPONSE, MSG_TYPES} from "../types/messages";
import {handleEvent} from "../utils/handleEvent";
import {EventEmitter} from "events";
import {awaitRPCResponse} from "../utils/awaitRPCResponse";

export default class WSClient extends EventEmitter {

    private _url = "";
    private _id?: number;
    private _username?: string;
    private _isAuth = false;
    private _loggerUsers: IWebSocket.UsersMap = {};

    /**
     * Client user ID on server
     */
    get id() {
        return this._id;
    }
    /**
     * Client username ID on server
     */
    get username() {
        return this._username;
    }
    /**
     * Map of logged users
     */
    get loggedUsers() {
        return this._loggerUsers;
    }

    /**
     * Status of authorization on server.
     * Authorization is required on server to call all the methods.
     */
    get isAuth() {
        return this._isAuth;
    }

    private _ws: IWebSocket.UnknownConnection | void = void 0;
    private _rpcsMap: Record<string, IWebSocket.RPC> = {};
    private _rpcsPool: Record<string | number, Promise<void>> = {};

    protected _logger: IWebSocket.Logger = console;
    
    constructor() {
        super();
    }

    destructor() {
        const {
            _ws,
        } = this;

        if (_ws) {
            this.disconnect()
                .then(() => {
                    if (!_ws) {
                        return;
                    }

                    _ws.off('message', this._onMessage);
                    _ws.off('close', this._onClose);
                })
                .catch(error => {
                    this._logger.error(`Got an error on disconnecting`, error);
                })
                .finally(() => {
                    this._ws = void 0;
                })
            ;
        }
    }

    /**
     * Connecting to WebSocket Server by specified URL
     *
     * @param url - url to WebSocket Server
     */
    async connect(url: string): Promise<void> {
        if (this._ws) {
            await this.disconnect();
        }

        let resolver;
        let rejecter;

        return new Promise((resolve, reject) => {
            this._ws = new WebSocket(url);
            this._url = url;
    
            if (this._ws) {
                resolver = resolve;
                rejecter = reject;

                this._ws.addEventListener('open', resolve);
                this._ws.addEventListener('close', reject);
            }
        })
        .then(() => {
            if (this._ws) {
                this._ws.removeEventListener('open', resolver);
                this._ws.removeEventListener('close', rejecter);

                this.emit(BASE_EVENTS.CONNECT);

                this._ws.on('message', this._onMessage);
                this._ws.on('close', this._onClose);
                this._ws.on('error', this._onError);
            }
        })
        .catch((err) => {
            this._logger.error(`Can't create connection`, err);

            if (this._ws) {
                this._ws.removeAllListeners();
                this._ws = void 0;
                this._url = void 0;
            }
        });
    }

    /**
     * Authorizing on server
     *
     * @param authData - Object with username and password
     * @param authData.username - Login to authorize on server
     * @param authData.password - Password to authorize on server
     */
    async auth(authData: IWebSocket.UserLoginData): Promise<void> {
        const {
            _ws,
            _isAuth,
        } = this;

        _assert_isWS(_ws);

        if (_isAuth) {
            return;
        }

        const promise = new Promise<Message_AUTH>((resolve, reject) => {
            const timeout = setTimeout(() => {
                _ws.off('message', listener);
                reject('Timeout');
                clearTimeout(timeout);
            }, 30000);
            const listener = (eventBuffer: IWebSocket.RawData) => {
                const [ msgTechInfo, /* targetId */, authMessage ] = this._handleEvent(eventBuffer);

                const { type: msgType } = getMsgInfo(msgTechInfo);

                if (msgType !== MSG_TYPES.AUTH) {
                    return;
                }

                if (_isAuthMessage(authMessage)) {
                    resolve(authMessage);
                }

                _ws.off('message', listener);
                clearTimeout(timeout);
            };

            _ws.on('message', listener);
        });

        const options: IWebSocket.FormMessageOptions = {
            targetID: MAIN_BUS_ID,
            type: MSG_TYPES.AUTH,
        }

        this._send([ authData.username, authData.password ], options);

        return promise
            .then((authMessage) => {
                const {
                    userData,
                    message,
                    isSuccess,
                } = authMessage;

                if (isSuccess) {
                    this._isAuth = isSuccess;
                    this._username = userData.username;
                    this._id = userData.id;

                    this.emit(BASE_EVENTS.AUTH, userData);
                }
                else {
                    this.emit(BASE_EVENTS.AUTH_FAILED, { reason: message });

                    throw new Error(message);
                }
            })
            .catch((err) => {
                this._isAuth = false;
                this._logger.log(err);
                this.emit(BASE_EVENTS.AUTH_FAILED, { reason: 'Timeout' });

                throw err;
            })
        ;
    }

    /**
     * Disconnects from server. Will wait to complete all pending RPC-tasks and then disconnects
     */
    async disconnect() {
        const {
            _rpcsPool,
        } = this;

        for (const pendingRPC of Object.values(_rpcsPool)) {
            await pendingRPC;
        }

        return new Promise((resolve) => {
            if (this._ws) {
                this._ws.addEventListener('close', resolve);
                this._ws.close();
            }
            else {
                resolve(null);
            }
        })
        .catch((err) => {
            this._logger.error(`Got an error on disconnecting`, err);
        })
        .finally(() => {
            if (this._ws) {
                this._ws.removeAllListeners();
            }

            this._rpcsMap = {};

            this._ws = void 0;
        });
    }

    private _onMessage = async (eventBuffer: IWebSocket.RawData) => {
        const [ msgTechInfo, /* targetId */, data ] = this._handleEvent(eventBuffer);
        const {
            type,
        } = getMsgInfo(msgTechInfo);

        switch (type) {
            case MSG_TYPES.ERROR: {
                this._logger.error(data);
                this.emit(BASE_EVENTS.ERROR, data);

                return;
            }
            case MSG_TYPES.RPC_CALL: {
                if (!_isRPCCallMessage(data)) {
                    this._logger.error(`[_onMessage] Received incorrect message to call RPC: ${JSON.stringify(data)}`);

                    return;
                }

                const {
                    uniqueID,
                    methodName,
                    params,
                } = data;
                const {
                    _ws,
                    _rpcsPool,
                } = this;

                _assert_isWS(_ws);

                const ongoingTask = _rpcsPool[uniqueID];

                if (ongoingTask) {
                    await ongoingTask;

                    // Must be unreachable due to auto-removing task after completing
                    if (_rpcsPool[uniqueID]) {
                        this._logger.error(`[_onMessage] Received duplicate uniqueID and can't complete it.`);

                        const result: Message_RPC_RESPONSE = {
                            uniqueID,
                            methodName,
                            isSuccess: false,
                            errorText: `Received uniqueID is already used`,
                        };

                        this._send(result, {
                            type: MSG_TYPES.RPC_RESPONSE,
                            targetID: MAIN_BUS_ID,
                        });

                        return;
                    }
                }

                let rpcCallPromise = this._callRPC(uniqueID, methodName, params, false).then((rpcResponse) => {
                    this._send(rpcResponse, {
                        type: MSG_TYPES.RPC_RESPONSE,
                        targetID: MAIN_BUS_ID,
                    });
                });

                _rpcsPool[uniqueID] = rpcCallPromise;

                rpcCallPromise = rpcCallPromise.then(() => {
                    delete _rpcsPool[uniqueID];
                });

                return;
            }
            case MSG_TYPES.EVENT: {
                if (!_isEventMessage(data)) {
                    this._logger.error(`[_onMessage] Received incorrect event message: ${JSON.stringify(data)}`);

                    return;
                }

                const {
                    event: eventName,
                    tag: eventTag,
                    data: eventData,
                } = data;

                this.emit(`${eventName}`, eventData);

                if (eventTag) {
                    this.emit(`${eventName}.${eventTag}`, eventData);
                }

                break;
            }
            case MSG_TYPES.BROADCAST:
            case MSG_TYPES.COMMON_MESSAGE: {
                if (!_isCommonMessage(data)) {
                    this._logger.error(`Received wrong format of message: ${JSON.stringify(data)}`);

                    return;
                }

                this.emit(BASE_EVENTS.MESSAGE, data);

                return;
            }
            default: {
                return;
            }
        }
    }

    private async _callRPC(
        uniqueID: string,
        methodName: string,
        params: Record<string | number, unknown> = {},
        isLocalCall: boolean
    ) {
        const {
            _rpcsMap,
        } = this;
        let result: unknown | void = void 0;

        if (isLocalCall) {
            const message: Message_RPC_CALL = {
                methodName,
                uniqueID,
                params,
            };
            const options: IWebSocket.FormMessageOptions = {
                type: MSG_TYPES.RPC_CALL,
                targetID: MAIN_BUS_ID,
            };

            const awaitPromise = awaitRPCResponse(this._ws, methodName, uniqueID, this._logger);

            this._send(message, options);

            return awaitPromise;
        }
        else {
            const localRPCMethod = _rpcsMap[methodName];

            if (!localRPCMethod) {
                this._logger.error(`Trying to call non-existing RPC method: ${methodName}`);

                return {
                    uniqueID,
                    methodName,
                    isSuccess: false,
                    errorText: "Method doesn't exist",
                } as Message_RPC_RESPONSE;
            }

            try {
                result = await localRPCMethod(params);
            }
            catch (err) {
                this._logger.error(`RPC "${methodName}" error:`, err);

                return {
                    uniqueID,
                    methodName,
                    isSuccess: false,
                    errorText: err instanceof Error ? err.message : err,
                } as Message_RPC_RESPONSE;
            }

            return {
                uniqueID,
                methodName,
                result,
                isSuccess: true,
            } as Message_RPC_RESPONSE;
        }
    }

    private _onClose = (/*event: CloseEvent*/) => {
        this.emit(BASE_EVENTS.DISCONNECTED);
    }

    private _onError = (/*event: ErrorEvent*/) => {
        this.emit(BASE_EVENTS.ERROR);
    }

    /**
     * Send some event to specified target by ID.
     *
     * @param targetID - target ID on the server
     * @param eventName - name of event
     * @param eventData - content of event
     * @param tag - additional tag to specify event to special category. Used to make possible to subscribe only on events with special tag
     */
    sendEvent(targetID: number, eventName: string | number, eventData: IWebSocket.ValidMessageData, tag?: string) {
        _assert_isAuth(this._isAuth);

        const eventMessage: Message_EVENT = {
            event: eventName,
            tag,
            data: eventData,
        }
        const msgOptions: IWebSocket.FormMessageOptions = {
            type: MSG_TYPES.EVENT,
            targetID,
            flags: 0,
        };

        this._send(eventMessage, msgOptions);
    }

    /**
     * Send some information to specified target by ID.
     *
     * @param data - message data
     * @param targetID - target ID on the server
     */
    send(data: IWebSocket.ValidMessageData, targetID: number): void {
        _assert_isAuth(this._isAuth);

        const options: IWebSocket.FormMessageOptions = {
            targetID,
            type: MSG_TYPES.COMMON_MESSAGE,
        };

        return this._send(data, options);
    }

    /**
     * Send some information to all clients currently connected to the server
     *
     * @param data - message data
     */
    broadcast(data: IWebSocket.ValidMessageData): void {
        _assert_isAuth(this._isAuth);

        const options: IWebSocket.FormMessageOptions = {
            targetID: MAIN_BUS_ID,
            type: MSG_TYPES.BROADCAST,
        };

        return this._send(data, options);
    }

    /**
     * Register local method to make it callable from clients, connected to server
     *
     * @param methodName - method which clients will use to call the method
     * @param method - method which will be called by clients request
     */
    async registerRPC(methodName: string, method: IWebSocket.RPC) {
        _assert_isAuth(this.isAuth);
        const {
            _rpcsMap,
        } = this;
        const rpcMethod = _rpcsMap[methodName];

        if (rpcMethod) {
            if (rpcMethod !== method) {
                this._logger.error(`Method is already exists: ${methodName}`);

                return false;
            }

            // same method was already added
            return true;
        }

        return this.call(RPC_NAME__registerRPC, { methodName, authorID: this._id })
            .then(() => {
                _rpcsMap[methodName] = method;

                return true;
            })
        ;
    }

    /**
     * Remove registered local method to prevent calling it remotely
     *
     * @param methodName - method which should be deleted
     */
    async unregisterRPC(methodName: string) {
        _assert_isAuth(this.isAuth);
        const {
            _rpcsMap,
        } = this;
        const rpcMethod = _rpcsMap[methodName];

        if (!rpcMethod) {
            return true;
        }

        return this.call(RPC_NAME__unregisterRPC, { methodName, authorID: this._id })
            .then(() => {
                delete _rpcsMap[methodName];

                return true;
            })
        ;
    }

    /**
     * Call remote procedure and receive result
     *
     * @param methodName - method which should be called
     * @param params - params which will be provided when calling method
     *
     * @return Result of method call (type can be specified with generic)
     * @throws Error if method gets Timeout or some another error
     */
    async call<T = unknown>(methodName: string, params?: Record<string | number, unknown>): Promise<T> {
        _assert_isAuth(this._isAuth);

        const localRPC = this._rpcsMap[methodName];

        if (localRPC) {
            return Promise.resolve(localRPC(params) as T);
        }

        const uniqueID = genUniqueID();

        return this._callRPC(uniqueID, methodName, params, true).then((rpcResponse) => {
            const {
                result,
                isSuccess,
                errorText,
            } = rpcResponse;

            if (isSuccess) {
                return result as T;
            }
            else {
                throw new Error(errorText);
            }
        });
    }

    private _send(data: IWebSocket.ValidMessageData, options: IWebSocket.FormMessageOptions) {
        _assert_isWS(this._ws);

        let msg = "";

        try {
            msg = formMsg(data, options);
        }
        catch(err) {
            this._logger.error(`Can't form message`, err);

            return;
        }

        this._ws.send(msg);
    }

    private async _updateClientsInfo(): Promise<void> {
        _assert_isWS(this._ws);
        _assert_isAuth(this._isAuth);

        this._loggerUsers = await this.call<IWebSocket.UsersMap>(RPC_NAME__getLoggedUsers);
    }

    private _handleEvent(eventBuffer: IWebSocket.RawData) {
        return handleEvent(eventBuffer, this._logger);
    }

    setLogger(newLogger: IWebSocket.Logger) {
        if (newLogger) {
            this._logger = newLogger;
        }
    }
}