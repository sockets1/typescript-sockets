
import type { IWebSocket } from "./types";

/**
 * Flags for messages.
 * Main thing: this values could be combined
 *
 * Max: 21 (31 - 5 for MSG_TYPES - 5 for DATA_TYPES)
 */
export const enum MSG_FLAGS {
    // Default
    BASE = 1 << 0,
    // In priority comparing with base
    PRIOR = 1 << 1,
    // Warning message
    WARNING = 1 << 2,
    // Message from server
    FROM_SERVER = 1 << 3,
}

/**
 * Types of message
 *
 * Max: 31 (2^5 - 1)
 */
export const enum MSG_TYPES {
    UNKNOWN = 0,
    AUTH = 1,
    COMMON_MESSAGE,

    DEFAULT, // send()
    BROADCAST, // broadcast()
    EVENT, // sendEvent()
    SUBSCRIBE, // subscribe()
    UNSUBSCRIBE, // unsubscribe()
    RPC_CALL, // rpc()
    RPC_RESPONSE,

    ERROR = 31,
}

/**
 * Type of raw message data
 *
 * Max: 31 (2^5 - 1)
 */
export const enum DATA_TYPES {
    UNKNOWN = 0,
    STRING = 1,
    OBJECT = 2,
    ARRAY = 3,
    BLOB = 4,
    ARRAY_BUFFER = 5,
    NUMBER_ARRAY = 6,
    //     data instanceof Int8Array || data instanceof Uint8Array || data instanceof Uint8ClampedArray
    //     || data instanceof Int16Array || data instanceof Int32Array || data instanceof Float32Array
    //     || data instanceof Uint16Array || data instanceof Uint32Array || data instanceof Float64Array
    //     || data instanceof BigInt64Array || data instanceof BigUint64Array
}

export interface Message_COMMON_MESSAGE {
    authorName: string;
    authorId: number;
    message: IWebSocket.ValidMessageData;
}

export interface Message_AUTH {
    message: string;
    isSuccess: boolean;
    userData?: IWebSocket.PublicUserData;
}

export interface Message_EVENT {
    event: string | number;
    tag?: string;
    data: IWebSocket.ValidMessageData;
}

export interface Message_RPC_CALL {
    uniqueID: string;
    methodName: string;
    params?: Record<string | number, unknown>;
}

type Message_RPC_RESPONSE<T = unknown> = _Message_RPC_RESPONSE_success<T> | _Message_RPC_RESPONSE_error;

interface _Message_RPC_RESPONSE_success<T> {
    uniqueID: string | number;
    methodName: string;
    isSuccess: true;
    errorText?: never;
    result: T;
}

interface _Message_RPC_RESPONSE_error {
    uniqueID: string | number;
    methodName: string;
    isSuccess: false;
    errorText: string;
    result?: never;
}

