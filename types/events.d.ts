
export const enum BASE_EVENTS {
    CONNECT = 'connected',
    AUTH = 'auth',
    AUTH_FAILED = 'authFailed',
    MESSAGE = 'message',
    DISCONNECTED = 'disconnected',
    ERROR = 'error',
}

export interface EventData_AUTH_FAILED {
    reason: string;
}