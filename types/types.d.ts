
import type WebSocket from "ws";
import {
    DATA_TYPES,
    MSG_TYPES,
} from "./messages";
import type {
    IncomingMessage as HTTPIncomingMessage
} from "http";

export namespace IWebSocket {
    export interface ServerOptions {
        port: number;
    }

    export type RawData = WebSocket.RawData
    export type IncomingRequest = HTTPIncomingMessage;
    export type UnknownConnection = WebSocket;

    export interface Connection extends WebSocket {
        id: number;
        user: UserData;
    }

    export type RPC = (params?: Object) => unknown;

    export interface FormMessageOptions {
        type: MSG_TYPES,
        flags?: number,
        targetID: number,
    }

    export interface IncomingMessageInfo {
        type: MSG_TYPES;
        dataType: DATA_TYPES;
        flags: number;
    }

    export interface PublicUserData {
        id: number;
        username: string;
    }

    export type UsersMap = Record<number, string>;

    export interface UserData extends PublicUserData {
        password: string;
    }

    export interface UserLoginData {
        username: string;
        password: string;
    }

    export interface RPCInfo {
        id: number;
        localMethod?: RPC;
    }

    export type ValidMessageData = unknown[] | Object | string | ArrayBufferLike | Blob | ArrayBufferView;

    export type OutgoingMessage = [ msgTechInfo: number, targetId: number, data: ValidMessageData ]

    export type IncomingMessage = [ msgTechInfo: number, targetId: number, data: ValidMessageData ];

    export interface Logger {
        log(...args: any[]): void;
        warn(...args: any[]): void;
        error(...args: any[]): void;
    }
}